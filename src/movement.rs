use std::collections::HashSet;

use crate::board::*;

impl Board {
    /// Moves the piece found at the origin to the destination if legal. Returns true
    /// if move was succesfull (i.e. legal)
    pub fn move_piece(&mut self, origin: Square, destination: Square) -> Option<GMove> {
        let legal_moves = self.get_legal_squares(origin);
        if !legal_moves.contains(&destination) {
            return None;
        }

        let old_origin = self[origin];
        let old_destination = self[destination];
        let mut capture = false;
        let old_origin_is_king = old_origin.piece_type == PieceType::King;

        if old_origin.piece_type == PieceType::Pawn {
            if destination.column() != origin.column() {
                if old_destination.is_none() {
                    return self.en_passant(origin, destination);
                } else {
                    capture = true;
                    self[destination] = old_origin;
                    self[origin] = NONE_PIECE;
                }
            } else {
                self[destination] = old_origin;
                self[origin] = NONE_PIECE;
            }
        } else if old_origin.piece_type == PieceType::King
            && (destination.column() - origin.column()).abs() == 2
        {
            return self.castle(origin, destination);
        } else {
            capture = !old_destination.is_none();
            self[destination] = old_origin;
            self[origin] = NONE_PIECE;
        }
        if old_origin_is_king {
            match self.current_player {
                Colour::Black => self.black_king = destination,
                Colour::White => self.white_king = destination,
            }
        }

        self.set_white_in_check(
            self.get_threatend_squares(Colour::White)
                .contains(&self.white_king),
        );
        self.set_black_in_check(
            self.get_threatend_squares(Colour::Black)
                .contains(&self.black_king),
        );

        if self.current_player_in_check() {
            self[origin] = old_origin;
            self[destination] = old_destination;

            if old_origin_is_king {
                match self.current_player {
                    Colour::Black => self.black_king = origin,
                    Colour::White => self.white_king = origin,
                }
            }

            self.set_white_in_check(
                self.get_threatend_squares(Colour::White)
                    .contains(&self.white_king),
            );
            self.set_black_in_check(
                self.get_threatend_squares(Colour::Black)
                    .contains(&self.black_king),
            );
            return None;
        }
        let checkmate = if self.check_checkmate() {
            GameState::Win(self.current_player)
        } else {
            GameState::None
        };
        self.current_player = self.current_player.flip();
        Some(GMove {
            piece: old_origin,
            origin,
            destination,
            capture,
            checkmate,
        })
    }

    fn en_passant(&mut self, origin: Square, destination: Square) -> Option<GMove> {
        let old_origin = self[origin];
        let old_destination = self[destination];
        let an_passant_sq = Square(destination.column(), origin.row());
        let old_an_passant = self[an_passant_sq];

        self[origin] = NONE_PIECE;
        self[destination] = old_origin;
        self[an_passant_sq] = NONE_PIECE;

        self.set_white_in_check(
            self.get_threatend_squares(Colour::White)
                .contains(&self.white_king),
        );
        self.set_black_in_check(
            self.get_threatend_squares(Colour::Black)
                .contains(&self.black_king),
        );

        if self.current_player_in_check() {
            self[origin] = old_origin;
            self[destination] = old_destination;
            self[an_passant_sq] = old_an_passant;

            self.set_white_in_check(
                self.get_threatend_squares(Colour::White)
                    .contains(&self.white_king),
            );
            self.set_black_in_check(
                self.get_threatend_squares(Colour::Black)
                    .contains(&self.black_king),
            );
            return None;
        }
        let checkmate = if self.check_checkmate() {
            GameState::Win(self.current_player)
        } else {
            GameState::None
        };

        self.current_player = self.current_player.flip();

        Some(GMove {
            piece: old_origin,
            origin,
            destination,
            capture: true,
            checkmate,
        })
    }

    /// Checks if the opposing player has been put into checkmate
    fn check_checkmate(&self) -> bool {
        let king = match self.current_player {
            Colour::Black => self.black_king,
            Colour::White => self.white_king,
        };
        self.get_threatend_squares(self.current_player.flip())
            .contains(&king)
            && !self.get_legal_squares(king).is_empty()
    }

    fn castle(&mut self, origin: Square, destination: Square) -> Option<GMove> {
        let old_origin = self[origin];
        let rook = self[match destination.column() {
            2 => Square(0, origin.row()),
            6 => Square(7, origin.row()),
            // SAFETY: for this function to be reached destination.column() must be either 2 or 6
            _ => unsafe { std::hint::unreachable_unchecked() },
        }];
        let rook_destination = match destination.column() {
            2 => Square(3, origin.row()),
            6 => Square(5, origin.row()),
            // SAFETY: for this function to be reached destination.column() must be either 2 or 6
            _ => unsafe { std::hint::unreachable_unchecked() },
        };

        self[origin] = NONE_PIECE;
        self[destination] = old_origin;
        self[rook_destination] = rook;
        match old_origin.colour {
            Colour::Black => self.black_king = destination,
            Colour::White => self.white_king = destination,
        }

        self.current_player = self.current_player.flip();
        Some(GMove {
            piece: old_origin,
            origin,
            destination,
            capture: false,
            checkmate: GameState::None,
        })
    }

    pub fn controlled_squares(&self, square: Square) -> Vec<Square> {
        let piece = self[square];
        if piece.colour != self.current_player {
            return Vec::new();
        }
        match piece.piece_type {
            PieceType::Rook | PieceType::Bishop | PieceType::Queen => {
                self.traverse_directions(square, piece.get_capture_directions())
            }
            PieceType::Pawn | PieceType::Knight | PieceType::King => {
                dirs_to_squares(square, piece.get_capture_directions())
            }
            PieceType::None => Vec::new(),
        }
    }

    pub fn get_legal_squares(&self, square: Square) -> HashSet<Square> {
        // First check that there is a piece and that it is of the correct colour
        let piece = self[square];
        if piece.colour != self.current_player {
            return HashSet::new();
        }

        let mut legal = HashSet::with_capacity(32);
        if piece.piece_type == PieceType::Pawn {
            legal.extend(self.check_pawn_move(square));
            legal.extend(self.check_en_passant(square));
        } else if piece.piece_type == PieceType::King {
            legal.extend(self.check_castling(square))
        }
        legal.extend(self.controlled_squares(square));
        let under_threat = self.get_threatend_squares(piece.colour);
        legal
            .into_iter()
            .filter(|p| self[*p].is_none() || self[*p].colour != piece.colour)
            .filter(|p| {
                if piece.piece_type == PieceType::King {
                    !under_threat.contains(p)
                } else {
                    true
                }
            })
            .collect()
    }

    fn traverse_directions(&self, origin: Square, directions: Vec<(i8, i8)>) -> Vec<Square> {
        let mut squares = Vec::with_capacity(32);
        for d in directions {
            let mut square = Square::from_square(origin, d.0, d.1);
            while square.in_bounds() && self[square].is_none() {
                squares.push(square);
                square = Square::from_square(square, d.0, d.1);
            }
            if square.in_bounds() {
                squares.push(square);
            }
        }
        squares.shrink_to_fit();
        squares
    }

    /// Checks the regular forwards pawn move. Also checks for the long initial jump move
    fn check_pawn_move(&self, square: Square) -> Vec<Square> {
        let pawn = self[square];
        let direction = pawn_direction(pawn.colour);
        if pawn.has_moved {
            if self[Square::from_square(square, 0, direction)].is_none() {
                return vec![Square::from_square(square, 0, direction)];
            } else {
                return Vec::new();
            }
        }
        // Check that the squares in front are empty
        let first_square_empty = self[Square::from_square(square, 0, direction)].is_none();
        let second_square_empty = self[Square::from_square(square, 0, 2 * direction)].is_none();
        if first_square_empty && second_square_empty {
            vec![
                Square::from_square(square, 0, direction),
                Square::from_square(square, 0, 2 * direction),
            ]
        } else if first_square_empty {
            vec![Square::from_square(square, 0, direction)]
        } else {
            return Vec::new();
        }
    }

    fn check_en_passant(&self, square: Square) -> Vec<Square> {
        let pawn = self[square];
        if pawn.has_moved || pawn.piece_type != PieceType::Pawn {
            return Vec::new();
        }
        let pawn = self[square];
        let en_passant_row = match pawn.colour {
            Colour::Black => 3,
            Colour::White => 4,
        };
        if square.row() != en_passant_row {
            return Vec::new();
        }
        let direction = pawn_direction(pawn.colour);
        let mut captures = Vec::with_capacity(1);
        if let Some(last_move) = self.moves.last() {
            if last_move.piece.piece_type == PieceType::Pawn
                && last_move.origin.row() == last_move.piece.get_home_row()
                && last_move.destination.row() == square.row()
                && (last_move.destination.column() - square.column()).abs() == 1
            {
                captures.push(Square(
                    last_move.destination.column(),
                    square.row() + direction,
                ));
            }
        }
        captures
    }

    fn check_castling(&self, square: Square) -> Vec<Square> {
        fn check_castling_squares(
            board: &Board,
            threatened: &HashSet<Square>,
            rook: Square,
            intermediate_squares: Vec<Square>,
        ) -> bool {
            !board[rook].has_moved
                && !threatened.contains(&rook)
                && intermediate_squares
                    .into_iter()
                    .all(|sq| board[sq].is_none() && !threatened.contains(&sq))
        }
        let king = self[square];
        if king.has_moved {
            return Vec::new();
        }
        let queenside_squares = vec![
            Square(1, square.row()),
            Square(2, square.row()),
            Square(3, square.row()),
        ];
        let kingside_squares = vec![Square(6, square.row()), Square(5, square.row())];

        let mut castle_squares = Vec::with_capacity(2);
        let threatened = self.get_threatend_squares(king.colour);

        // check queenside castling
        let rook_sq = Square(0, square.row());
        if check_castling_squares(self, &threatened, rook_sq, queenside_squares) {
            castle_squares.push(Square(KING_HOME_COLUMN - 2, square.row()))
        }

        // kingside castling
        let rook_sq = Square(7, square.row());
        if check_castling_squares(self, &threatened, rook_sq, kingside_squares) {
            castle_squares.push(Square(KING_HOME_COLUMN + 2, square.row()))
        }

        castle_squares
    }

    /// Gets the squares that are "threatened" according to the player of the passed colour.
    /// For example, if this function is passed the colour White, it will return all
    /// squares threatened by Black pieces.
    fn get_threatend_squares(&self, colour: Colour) -> HashSet<Square> {
        let mut set = HashSet::with_capacity(64);
        for (o_sq, piece) in self.iter() {
            if piece.colour != colour {
                set.extend(self.controlled_squares(o_sq));
            }
        }
        set
    }
}

impl Piece {
    pub(crate) fn get_capture_directions(&self) -> Vec<(i8, i8)> {
        self.flip_if_pawn(self.piece_type.get_capture_directions())
    }

    fn flip_if_pawn(&self, dirs: Vec<(i8, i8)>) -> Vec<(i8, i8)> {
        if self.piece_type == PieceType::Pawn && self.colour == Colour::Black {
            dirs.into_iter().map(|d| flip_row_dir(d)).collect()
        } else {
            dirs
        }
    }

    fn get_home_row(&self) -> i8 {
        self.piece_type.get_home_row(self.colour)
    }
}

impl PieceType {
    /// Gets the default move directions for the pieces. Must be evaluated and
    /// processed by a context that it aware of rules and board state
    fn get_move_directions(&self) -> Vec<(i8, i8)> {
        match self {
            PieceType::Pawn => vec![(0, 1)],
            PieceType::Rook => get_col_directions().into(),
            PieceType::Knight => vec![
                (2, 1),
                (2, -1),
                (-2, 1),
                (-2, -1),
                (1, 2),
                (1, -2),
                (-1, 2),
                (-1, -2),
            ],
            PieceType::Bishop => get_diag_directions().into(),
            PieceType::Queen | PieceType::King => {
                let mut dirs = Vec::with_capacity(8);
                dirs.extend(get_col_directions());
                dirs.extend(get_diag_directions());
                dirs
            }
            PieceType::None => Vec::new(),
        }
    }

    /// Gets the directions that a piece can capture in.
    /// Must be evaluated by a board-aware context.
    fn get_capture_directions(&self) -> Vec<(i8, i8)> {
        match self {
            PieceType::Pawn => vec![(1, 1), (-1, 1)],
            PieceType::Rook
            | PieceType::Knight
            | PieceType::Bishop
            | PieceType::Queen
            | PieceType::King => self.get_move_directions(),
            PieceType::None => Vec::new(),
        }
    }

    fn get_home_row(&self, colour: Colour) -> i8 {
        let (base_row, pawn_row) = match colour {
            Colour::Black => (7, 6),
            Colour::White => (0, 1),
        };
        match self {
            PieceType::Pawn => pawn_row,
            PieceType::Rook
            | PieceType::Knight
            | PieceType::Bishop
            | PieceType::Queen
            | PieceType::King => base_row,
            PieceType::None => -1,
        }
    }
}

#[inline]
fn get_col_directions() -> [(i8, i8); 4] {
    [(1, 0), (0, 1), (-1, 0), (0, -1)]
}

#[inline]
fn get_diag_directions() -> [(i8, i8); 4] {
    [(1, 1), (1, -1), (-1, 1), (-1, -1)]
}

#[inline]
fn pawn_direction(colour: Colour) -> i8 {
    match colour {
        Colour::Black => -1,
        Colour::White => 1,
    }
}

fn flip_row_dir(direction: (i8, i8)) -> (i8, i8) {
    (direction.0, -1 * direction.1)
}

/// Takes an origin square and some directions and returns the squares that are "in bounds"
/// of the board
fn dirs_to_squares(origin: Square, directions: Vec<(i8, i8)>) -> Vec<Square> {
    directions
        .into_iter()
        .map(|d| Square::from_square(origin, d.0, d.1))
        .filter(|sq| sq.in_bounds())
        .collect()
}

#[cfg(test)]
mod test_game {
    use crate::prelude::Board;

    #[test]
    fn test_speedrun_to_checkmate() {
        let mut game = Board::new_game();
    }
}
