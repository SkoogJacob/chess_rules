//! This crate re-exports the key public classes and impls

pub use crate::board::{Board, Colour, GMove, Piece, PieceType, Square};
pub use crate::movement;
