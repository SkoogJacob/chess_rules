/// This module contains Structs and methods to do
/// with the board and pieces
pub mod board;
/// This mod contains functions that concern the movement
/// of pieces and the legality of those moves
pub mod movement;
/// This module contains key methods to instantiate a board and
/// make calls that check the legality of a particular move
pub mod prelude;
