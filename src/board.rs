use std::{
    hash::Hash,
    ops::{Index, IndexMut},
    usize,
};

pub const KING_HOME_COLUMN: i8 = 4;
pub(crate) const NONE_PIECE: Piece = Piece::new(PieceType::None, Colour::White);

#[repr(packed)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Square(pub(crate) i8, pub(crate) i8);

impl Square {
    /// Creates a new square by moving from it as the origin according to the given directions.
    /// Positive column direction means it moves to the right, and positive row direction means
    /// it moves towards black's side of the board
    #[inline]
    pub fn from_square(origin: Square, col_direction: i8, row_direction: i8) -> Self {
        Square(
            origin.column() + col_direction,
            origin.row() + row_direction,
        )
    }

    #[inline]
    pub fn in_bounds(&self) -> bool {
        self.column() >= 0 && self.column() < 8 && self.row() >= 0 && self.row() < 8
    }

    #[inline]
    pub fn column(&self) -> i8 {
        self.0
    }
    #[inline]
    pub fn row(&self) -> i8 {
        self.1
    }
}

impl From<(i8, i8)> for Square {
    fn from(value: (i8, i8)) -> Self {
        Self(value.0, value.1)
    }
}

impl Hash for Square {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let num: i8 = self.column() << 4 | self.row();
        state.write_i8(num)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PieceType {
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King,
    None,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Colour {
    Black,
    White,
}

impl Colour {
    pub fn flip(&self) -> Self {
        match self {
            Colour::Black => Colour::White,
            Colour::White => Colour::Black,
        }
    }
}

#[repr(packed)]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Piece {
    pub(crate) piece_type: PieceType,
    pub(crate) colour: Colour,
    pub(crate) has_moved: bool,
}

impl Piece {
    pub const fn new(piece_type: PieceType, colour: Colour) -> Self {
        Self {
            piece_type,
            colour,
            has_moved: false,
        }
    }

    pub fn is_none(&self) -> bool {
        // TODO: check how to do enum comparisons in const
        self.piece_type == PieceType::None
    }
}

#[repr(packed)]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct GMove {
    pub piece: Piece,
    pub origin: Square,
    pub destination: Square,
    pub capture: bool,
    pub checkmate: GameState,
}

/// This struct describes if the current game has "ended".
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum GameState {
    /// Means that the player with the colour contained in the variant has won.
    /// Conversely, this means that the other player is checkmated.
    Win(Colour),
    /// This means that the game is in a stalemate state where the current player cannot
    /// make any legal moves
    Stalemate,
    /// This variant means that the game is proceeding "business as usual"
    None,
}

pub struct Board {
    pub(crate) pieces: [[Piece; 8]; 8],
    pub(crate) current_player: Colour,
    in_check: [bool; 2],
    pub(crate) white_king: Square,
    pub(crate) black_king: Square,
    pub(crate) moves: Vec<GMove>,
}

/// Creates an initial column for chess based on the piece that's passed in
macro_rules! make_init_column {
    ($pt:expr) => {
        [
            Piece::new($pt, Colour::White),
            Piece::new(PieceType::Pawn, Colour::White),
            NONE_PIECE,
            NONE_PIECE,
            NONE_PIECE,
            NONE_PIECE,
            Piece::new(PieceType::Pawn, Colour::Black),
            Piece::new($pt, Colour::Black),
        ]
    };
}

impl Board {
    pub fn new_game() -> Board {
        Self {
            pieces: Board::generate_board(),
            current_player: Colour::White,
            in_check: [false, false],
            white_king: Square(KING_HOME_COLUMN, 0),
            black_king: Square(KING_HOME_COLUMN, 7),
            moves: Vec::with_capacity(256),
        }
    }

    pub fn iter(&self) -> BoardIterator {
        BoardIterator::new(self)
    }

    pub fn material_scores(&self) -> (u8, u8) {
        let mut white_score = 0;
        let mut black_score = 0;

        for (_, piece) in self.iter() {
            let value = match piece.piece_type {
                PieceType::Pawn => 1,
                PieceType::Rook => 5,
                PieceType::Knight => 3,
                PieceType::Bishop => 3,
                PieceType::Queen => 7,
                PieceType::King => 0,
                PieceType::None => 0,
            };
            match piece.colour {
                Colour::Black => black_score += value,
                Colour::White => white_score += value,
            };
        }
        (white_score, black_score)
    }

    fn generate_board() -> [[Piece; 8]; 8] {
        [
            make_init_column!(PieceType::Rook),
            make_init_column!(PieceType::Knight),
            make_init_column!(PieceType::Bishop),
            make_init_column!(PieceType::Queen),
            make_init_column!(PieceType::King),
            make_init_column!(PieceType::Bishop),
            make_init_column!(PieceType::Knight),
            make_init_column!(PieceType::Rook),
        ]
    }

    pub fn current_player_in_check(&self) -> bool {
        match self.current_player {
            Colour::Black => self.black_in_check(),
            Colour::White => self.white_in_check(),
        }
    }

    #[inline]
    pub fn white_in_check(&self) -> bool {
        self.in_check[0]
    }

    #[inline]
    pub fn set_white_in_check(&mut self, in_check: bool) {
        self.in_check[0] = in_check
    }

    #[inline]
    pub fn black_in_check(&self) -> bool {
        self.in_check[1]
    }

    #[inline]
    pub fn set_black_in_check(&mut self, in_check: bool) {
        self.in_check[1] = in_check
    }
}

impl Index<Square> for Board {
    type Output = Piece;

    fn index(&self, idx: Square) -> &Self::Output {
        if !idx.in_bounds() {
            return &NONE_PIECE;
        }
        let index = (idx.0 as usize, idx.1 as usize);
        &self.pieces[index.0][index.1]
    }
}

impl IndexMut<Square> for Board {
    fn index_mut(&mut self, idx: Square) -> &mut Self::Output {
        let index = (idx.0 as usize, idx.1 as usize);
        &mut self.pieces[index.0][index.1]
    }
}

pub struct BoardIterator<'board>(i8, i8, &'board Board);

impl<'board> BoardIterator<'board> {
    pub fn new(board: &'board Board) -> Self {
        Self(0, 0, board)
    }
}

impl<'board> Iterator for BoardIterator<'board> {
    type Item = (Square, Piece);

    fn next(&mut self) -> Option<Self::Item> {
        if self.0 == 8 {
            return None;
        }
        let square = Square(self.0, self.1);
        if self.1 == 7 {
            self.0 += 1;
            self.1 = 0;
        } else {
            self.1 += 1;
        }
        let board = self.2;
        Some((square, board[square]))
    }
}
